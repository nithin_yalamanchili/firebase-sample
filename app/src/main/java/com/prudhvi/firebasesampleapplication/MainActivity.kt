package com.prudhvi.firebasesampleapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db = FirebaseFirestore.getInstance()


        val create_recore=findViewById<Button>(R.id.button3)

        create_recore.setOnClickListener {
            // Create a new user with a first and last name


            val user:HashMap<String,Any> = HashMap<String,Any>()
            user.put("first", "Ada")
            user.put("last", "Lovelace")
            user.put("born", "1993")

// Add a new document with a generated ID
            db.collection("users")
                    .add(user)
                    .addOnSuccessListener {
                        documentReference -> Toast.makeText(applicationContext, "Recored Created",Toast.LENGTH_SHORT).show()}
                    .addOnFailureListener {
                        e -> Toast.makeText(applicationContext, "Error in Creation Created",Toast.LENGTH_SHORT).show()}
        }






    }
}
